upload_content = [
  { content = "To infinity, and beyond! (created via our dynamic block)", file = "/terraform/test1.txt", executable = true },
  { content = "Good news, everyone! (created via our dynamic block)", file = "/terraform/test2.txt", executable = true },
  { content = "Ain't nothing more beautiful than the Noxian plains... excepting a rich corpse.", file = "/terraform/test3.txt", executable = true },
  { content = "I find courage unpredictable. It's total insanity you can rely on!", file = "/terraform/test4.txt", executable = true },
  { content = "Oh, we find 'em, tie 'em up, then they's like a piñata full of meat.", file = "/terraform/test5.txt", executable = true },
  { content = "My brain is on fire, my soul is strong, and my lizard is hungry!", file = "/terraform/test6.txt", executable = true },
]
